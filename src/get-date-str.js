'use strict';

function patZero(str, n) {
  if (n <= 0) {
    return str;
  }
  const zero = '0'.repeat(n-1);
  const ret = (zero+str).slice(-n);
  return ret;
}

exports.getDateStr = function(sp) {
  const date = new Date();
  const str = [];
  str.push(patZero(date.getFullYear(), 2));
  str.push(patZero(date.getMonth()+1, 2));
  str.push(patZero(date.getDate(), 2));
  str.push(patZero(date.getHours(), 2));
  str.push(patZero(date.getMinutes(), 2));
  str.push(patZero(date.getSeconds(), 2));
  const ret = str.join(sp);
  return ret;
}
