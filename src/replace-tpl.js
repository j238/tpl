'use strict';

function makeStdRegExp(i) {
  return new RegExp(`\\$\\{[\s\t]*${i}[\s\t]*\\}`, 'g');
}

exports.replaceTpl = function(config, tplData, owArgs = {}) {
  for (const i in config) {
    const s = (owArgs[i]!==undefined)?owArgs[i]:config[i];
    if (s) {
      tplData = tplData.replace(makeStdRegExp(i), s);
    }
  }
  return tplData;
}
