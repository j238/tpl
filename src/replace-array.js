'use strict';

function makeArrayRegExp(i) {
  return new RegExp(`\\$\\[[\s\t]*${i}[\s\t]*\\]`, 'g');
}

function replaceArrs(tpl, arrs) {
  const kvs = [];
  for (const k in tpl) {
    const v = tpl[k];
    let isPushed = false;
    for (const j in arrs) {
      if (k.match(makeArrayRegExp(j))) {
        const arr = arrs[j];
        const kv = {k: k, v: v, j: j, arr: arr};
        for (const a of arr) {
          const tpla = tpl[a];
          if (tpla !== undefined) {
            if (!kv.ex) {
              kv.ex = {};
            }
            kv.ex[a] = tpla;
          }
        }
        kvs.push(kv);
        isPushed = true;
      }
    }
    if (!isPushed) {
      if (v instanceof Object) {
        replaceArrs(v, arrs);
      }
    }
  }
  for (const kv of kvs) {
    for (const a of kv.arr) {
      const ii = kv.k.replace(makeArrayRegExp(kv.j), a);
      let s = JSON.stringify(kv.v);
      s = s.replace(makeArrayRegExp(kv.j), a);
      tpl[ii] = JSON.parse(s);
      const tplii = tpl[ii];
      if (kv.ex && tplii instanceof Object && kv.v instanceof Object) {
        const kvexa = kv.ex[a];
        if (kvexa) {
          tpl[ii] = mergeDeeply(tplii, kvexa, {concatArray: true});
        }
      }
    }    
    delete tpl[kv.k];
  }
  if (kvs.length) {
    replaceArrs(tpl, arrs);
  }
}

exports.replaceArray = function(config, tplData) {
  const arrs = {};
  for (const i in config) {
    const s = config[i];
    if (s && Array.isArray(s)) {
      arrs[i] = s;
    }
  }  
  if (Object.keys(arrs).length) {
    const tpl = JSON.parse(tplData);
    replaceArrs(tpl, arrs);
    tplData = JSON.stringify(tpl);
  }
  return tplData;
}

function mergeDeeply(target, source, opts) {
  const isObject = obj => obj && typeof obj === 'object' && !Array.isArray(obj);
  const isConcatArray = opts && opts.concatArray;
  let result = Object.assign({}, target);
  if (isObject(target) && isObject(source)) {
      for (const [sourceKey, sourceValue] of Object.entries(source)) {
          const targetValue = target[sourceKey];
          if (isConcatArray && Array.isArray(sourceValue) && Array.isArray(targetValue)) {
              result[sourceKey] = targetValue.concat(...sourceValue);
          }
          else if (isObject(sourceValue) && target.hasOwnProperty(sourceKey)) {
              result[sourceKey] = mergeDeeply(targetValue, sourceValue, opts);
          }
          else {
              Object.assign(result, {[sourceKey]: sourceValue});
          }
      }
  }
  return result;
}
