#!/usr/bin/env node
'use strict';
const fs = require('fs');
const getDestName = require('./get-dest-name').getDestName;
const getOverwriteArg = require('./get-ow-arg').getOverwriteArg;
const replaceTpl = require('./replace-tpl').replaceTpl;

try {
  const tplFile = process.argv[2];
  if (!tplFile) {
    throw new Error('not found tpl file name');
  }
  const destFile = getDestName(tplFile);
  const owArgs = getOverwriteArg(process.argv[3]);
  let configFile = process.argv[4];
  if (!configFile) {
    configFile = 'tpl.config.json';
  }

  let tplData = fs.readFileSync(tplFile, 'utf8');
  const configData = fs.readFileSync(configFile, 'utf8');
  const config = JSON.parse(configData);

  tplData = replaceTpl(config, tplData, owArgs);

  fs.writeFileSync(destFile, tplData);
  console.log('output:', destFile);
} catch(err) {
  console.log(err.message);
}