'use strict';
const fs = require('fs');

const counterFileName = './counter.json';
exports.getCounter = function() {
  try {
    const counterData = fs.readFileSync(counterFileName, 'utf8');
    const counter = JSON.parse(counterData);
    //counter.counter++;
    const counterJson = JSON.stringify(counter, null, 2);
    fs.writeFileSync(counterFileName, counterJson);  
    return counter.counter;
  } catch(err) {
    console.error(err);
    return 1;
  }
}
