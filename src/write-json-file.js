'use strict';
const fs = require('fs');

exports.writeJsonFile = function(tplData, destFile) {
  const destData = fs.readFileSync(destFile, 'utf8');
  const dest = JSON.parse(destData);
  const tpl = JSON.parse(tplData);
  for (const i in tpl) {
    if (i in dest) {
      dest[i] = tpl[i];
    }
  }
  const content = JSON.stringify(dest, null, 2);
  fs.writeFileSync(destFile, content);
}