'use strict';

exports.getDestName = function(tplName) {
  const tplNames = tplName.split('.');
  let ok = false;
  const destNames = [];
  for (const s of tplNames) {
    if ((!ok) && s === 'tpl') {
      ok = true;
      continue;
    }
    destNames.push(s);
  }
  if (!ok) {
    throw new Error('not found ext ".tpl"');
  }
  const destName = destNames.join('.');  
  return destName;
}
