
'use strict';

const getDateStr = require('./get-date-str').getDateStr;
const getCounter = require('./get-counter').getCounter;

exports.getOverwriteArg = function(arg) {
  const owArgs = {};
  if (arg) {
    const arg3s = arg.split(',');
    for (const item of arg3s) {
      const values = item.split('=');
      if (values.length == 2) {
        let value = String(values[1]);
        const dateMatch = value.match(/__Date__(\S*)/);
        const counterMatch = value.match(/__Counter__/);
        if (dateMatch) {
          owArgs[values[0]] = getDateStr(dateMatch[1]);
        } else if (counterMatch) {
          owArgs[values[0]] = getCounter();
        } else {
          owArgs[values[0]] = value;
        }
      }
    }
  }  
  return owArgs;
}
